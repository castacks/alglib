/*
* Copyright (c) 2016 Carnegie Mellon University, Author <sanjiban@cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/
/* Copyright 2015 Sanjiban Choudhury
 * minqp_d_bc1.cpp
 *
 *  Created on: May 9, 2015
 *      Author: Sanjiban Choudhury
 */

#include <iostream>
#include "alglib/alglib_utils.h"

using namespace alglib;
using namespace ca::alglib_utils;



int main(int argc, char **argv) {
  Eigen::MatrixXd A(2,2);
  A << 2, 0,
      0, 2;
  Eigen::VectorXd b(2);
  b << -6, -4;
  Eigen::VectorXd lb(2);
  lb << 0, 0;
  Eigen::VectorXd ub(2);
  ub << 2.5, 2.5;
  Eigen::VectorXd x0(2);
  x0 << 0, 1;
  Eigen::VectorXd x;

  QPOptions opt;

  bool status = SolveBoxQP(A, b, lb, ub, x0, opt, x);

  if(status == false)
  {
     printf("ERROR, failed to solve quadratic problem\n");
  }

  std::cout<<x<<"\n";
  return 0;
}


