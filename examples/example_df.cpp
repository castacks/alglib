/*
* Copyright (c) 2016 Carnegie Mellon University, Author <sanjiban@cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/

/* Copyright 2015 Sanjiban Choudhury
 * example_df.cpp
 *
 *  Created on: Dec 19, 2015
 *      Author: Sanjiban Choudhury
 */


#include <iostream>
#include "alglib/dataanalysis.h"

using namespace std;

int main () {
  cout << "Welcome." << endl;

  alglib::dfreport rep;
  alglib::decisionforest df;
  alglib::ae_int_t info;

  cout << "Created forest, and report." << endl;

  alglib::real_2d_array train("[[0,1,0,0.7],[0,0,1,0.4],[0.1,0,1,0.3]]");

  cout << "Created training set." << endl;

  alglib::real_2d_array test = "[[0,1,0,0.6]]";

  cout << "Training." << endl;
  alglib::dfbuildrandomdecisionforest(train, 3, 3, 1, 3, 1, info, df, rep);


  double error = alglib::dfavgrelerror(df, test, 1);
  cout << "Average error is " << error << endl;

  alglib::real_1d_array feat = "[0,1,0]";
  alglib::real_1d_array output;

  alglib::dfprocess(df, feat, output);
  cout << "Output is " << output[0] << endl;
  // Can serialize using alglib::dfserialize and dfunserialize
  return 0;
}
