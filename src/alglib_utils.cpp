/*
* Copyright (c) 2016 Carnegie Mellon University, Author <sanjiban@cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/

/* Copyright 2015 Sanjiban Choudhury
 * alglib_utils.cpp
 *
 *  Created on: May 9, 2015
 *      Author: Sanjiban Choudhury
 */

#include "alglib/alglib_utils.h"
#include <iostream>
using namespace alglib;

namespace ca {

namespace alglib_utils {

void ConvertEigenVecToAlgLib(const Eigen::VectorXd &input, alglib::real_1d_array &output) {
  output.setcontent(input.rows(), input.data());
}

void ConvertEigenVecToAlgLib(const Eigen::VectorXi &input, alglib::integer_1d_array &output) {
  output.setcontent(input.rows(), (long int*)input.data());
}

void ConvertEigenMatToAlgLib(const Eigen::MatrixXd &input, alglib::real_2d_array &output) {
  Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> input_rm = input;
  output.setcontent(input.rows(), input.cols(), input_rm.data());
}

void ConvertAlgLibToEigenVec(const alglib::real_1d_array &input, Eigen::VectorXd &output) {
  output = Eigen::Map<const Eigen::VectorXd> (input.getcontent(), input.length());
}

void ConvertAlgLibToEigenMat(const alglib::real_2d_array &input, Eigen::MatrixXd &output) {
  output = Eigen::Map<const Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> > (input[0], input.rows(), input.cols());
}

void ProjectToSymmetric(const alglib::real_2d_array &input, alglib::real_2d_array &output) {
  output.setlength(input.rows(), input.cols());
  for (int i = 0 ; i < input.rows(); i++)
    for (int j = 0; j < input.cols(); j++)
      output[i][j] = 0.5*(input[i][j] + input[j][i]);
}

bool SolveBoxQP(const Eigen::MatrixXd &A, const Eigen::VectorXd &b,
                const Eigen::VectorXd &lb, const Eigen::VectorXd &ub,
                const Eigen::VectorXd &x0,
                const QPOptions &opt,
                Eigen::VectorXd &x) {

//  std::cout<<"A:\n"<<A<<"\n";
//  std::cout<<"b:\n"<<b<<"\n";
//  std::cout<<"lb:\n"<<lb<<"\n";
//  std::cout<<"ub:\n"<<ub<<"\n";
//  std::cout<<"x0:\n"<<x0<<"\n";

  //  std::cout<<"b:\n"<<b.rows()<<" "<<b.cols()<<"\n";

  real_2d_array Aunsym_, A_;
  real_1d_array b_, lb_, ub_, x0_, x_, s_;
  Eigen::VectorXd s = Eigen::VectorXd::Ones(b.rows());

  try
  {

     //convert eigen to alglib datatypes
     ConvertEigenMatToAlgLib(A, Aunsym_);
     ConvertEigenVecToAlgLib(b, b_);
     ConvertEigenVecToAlgLib(lb, lb_);
     ConvertEigenVecToAlgLib(ub, ub_);
     ConvertEigenVecToAlgLib(x0, x0_);
     ConvertEigenVecToAlgLib(s, s_);

     ProjectToSymmetric(Aunsym_, A_);

     minqpstate state;
     minqpreport rep;

     // create solver, set quadratic/linear terms
     minqpcreate(x0.rows(), state);
     minqpsetquadraticterm(state, A_);
     minqpsetlinearterm(state, b_);
     minqpsetstartingpoint(state, x0_);
     minqpsetbc(state, lb_, ub_);
     minqpsetscale(state, s_);

     // solve problem with QuickQP solver, default stopping criteria are used
     minqpsetalgoquickqp(state, opt.eps_g, opt.eps_f, opt.eps_x, opt.outer_iters, opt.use_newton);
     minqpoptimize(state);
     minqpresults(state, x_, rep);
     ConvertAlgLibToEigenVec(x_, x);

 }
 catch(const alglib::ap_error & e)
 {
     printf("SolveBoxQP: ERROR: Exception thrown: %s\n", e.msg.c_str());
     return false;
 }

 return true;

 // std::cout<<"x:\n"<<x<<"\n";

}

}
}


