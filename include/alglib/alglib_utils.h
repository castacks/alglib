/*
* Copyright (c) 2016 Carnegie Mellon University, Author <sanjiban@cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/

/* Copyright 2015 Sanjiban Choudhury
 * alglib_utils.h
 *
 *  Created on: May 9, 2015
 *      Author: Sanjiban Choudhury
 */

#ifndef ALGLIB_INCLUDE_ALGLIB_ALGLIB_UTILS_H_
#define ALGLIB_INCLUDE_ALGLIB_ALGLIB_UTILS_H_

#include <Eigen/Dense>
#include "alglib/stdafx.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "alglib/optimization.h"
#include <Eigen/Dense>
#include "alglib/alglib_utils.h"

namespace ca {

/**
 * \brief Wrapper functions for performing operations using alglib.
 */
namespace alglib_utils {

/**
 * \brief options for quadratic program solver
 */
struct QPOptions {
  double eps_g;
  double eps_f;
  double eps_x;
  double outer_iters;
  double use_newton;

  /**
   * \brief Default constructor
   */
  QPOptions()
      : eps_g(0),
        eps_f(0),
        eps_x(0),
        outer_iters(0),
        use_newton(true) {
  }
};

/**
 * \brief Converts an Eigen vector to a alglib vector of matching dimension
 * @param input the Eigen vector
 * @param output the alglib vector
 */
void ConvertEigenVecToAlgLib(const Eigen::VectorXd &input,
                             alglib::real_1d_array &output);

void ConvertEigenVecToAlgLib(const Eigen::VectorXi &input,
                             alglib::integer_1d_array &output);
/**
 * \brief Converts an Eigen matrix to a alglib matrix of matching dimension
 * @param input the Eigen matrix
 * @param output the alglib matrix
 */
void ConvertEigenMatToAlgLib(const Eigen::MatrixXd &input,
                             alglib::real_2d_array &output);

/**
 * \brief Converts an alglib vector to an Eigen vector of matching dimension
 * @param input the alglib vector
 * @param output the Eigen vector
 */
void ConvertAlgLibToEigenVec(const alglib::real_1d_array &input,
                             Eigen::VectorXd &output);

/**
 * \brief Converts an alglib matrix to an Eigen matrix of matching dimension
 * @param input the alglib matrix
 * @param output the Eigen matrix
 */
void ConvertAlgLibToEigenMat(const alglib::real_2d_array &input,
                             Eigen::MatrixXd &output);

/**
 * \brief Calculates the symmetric, positive definite, projection of a matrix.
 * @param input the matrix to calculate the projection from
 * @param output the projection
 */
void ProjectToSymmetric(const alglib::real_2d_array &input,
                        alglib::real_2d_array &output);

/**
 * \brief Solves a quadratic program with additional upper and lower bounds (box constraints)
 * @param A The A in linear constraint equation Ax = b
 * @param b The b in linear constraint equation Ax = b
 * @param lb The lower bound constraint such that x >= lb
 * @param ub The upper bound constraint such that x <= ub
 * @param x0 Initial solution
 * @param opt Additional quadratic program options
 * @param x (output) the solution returned by the solver
 * @return true if successful in finding a solution
 */
bool SolveBoxQP(const Eigen::MatrixXd &A, const Eigen::VectorXd &b,
                const Eigen::VectorXd &lb, const Eigen::VectorXd &ub,
                const Eigen::VectorXd &x0, const QPOptions &opt,
                Eigen::VectorXd &x);

}

}

#endif /* ALGLIB_INCLUDE_ALGLIB_ALGLIB_UTILS_H_ */
